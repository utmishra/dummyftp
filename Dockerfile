FROM maven:3.9.7-eclipse-temurin-22-alpine AS build
WORKDIR /app
COPY . .
RUN mvn clean package

FROM eclipse-temurin:22-jdk-alpine AS builder
WORKDIR /app
COPY --from=build /app/target/*.jar /app/app.jar

EXPOSE 21
ENTRYPOINT ["java", "-jar", "app.jar"]
