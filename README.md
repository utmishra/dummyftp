# DummyFTP
Spring Boot powered Apached FTP server.

> ⚠️ This project is for experimental purpose only. 
> 
> At the moment the connection runs on plain FTP, which is not secure.
> 
> Use with caution

## How to start
- Ensure that Docker is installed
- Set up the FTP user credentials `secret.properties` located in `src/main/resources`
- The properties should look like:
```properties
ftpserver.userName=<userName>
ftpserver.userPassword=<userPassword>
```
- You may also need to modify `application.properties` in `src/main/resources` to update the following FTP configurations:
```properties
ftpserver.homeDirectory=ftp_root
ftpserver.enableFlag=true
ftpserver.writePermission=true
ftpserver.idleTime=30000
```
- Run:
```shell
docker-compose up --build -d
```
- Access FTP server at `localhost:21`