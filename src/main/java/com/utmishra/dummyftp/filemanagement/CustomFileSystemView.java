package com.utmishra.dummyftp.filemanagement;

import org.apache.ftpserver.filesystem.nativefs.impl.NativeFileSystemView;
import org.apache.ftpserver.ftplet.FtpException;
import org.apache.ftpserver.ftplet.FtpFile;
import org.apache.ftpserver.ftplet.User;


public class CustomFileSystemView extends NativeFileSystemView {

    private final String homeDirectory;

    public CustomFileSystemView(User user, String homeDirectory) throws FtpException {
        super(user);
        this.homeDirectory = homeDirectory;
    }

    @Override
    public FtpFile getHomeDirectory() {
        return getFile(homeDirectory);
    }
}