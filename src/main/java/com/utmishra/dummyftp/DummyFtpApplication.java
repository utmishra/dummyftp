package com.utmishra.dummyftp;

import com.utmishra.dummyftp.config.FtpConfigProperties;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

@SpringBootApplication
@EnableConfigurationProperties(FtpConfigProperties.class)
public class DummyFtpApplication {

	public static void main(String[] args) {
		SpringApplication.run(DummyFtpApplication.class, args);
	}

}
