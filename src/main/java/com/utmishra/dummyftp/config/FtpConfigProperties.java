package com.utmishra.dummyftp.config;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix = "ftpserver")
public record FtpConfigProperties(
        String userName,
        String userPassword,
        String homeDirectory,
        boolean enableFlag,
        boolean writePermission,
        int maxLoginNumber,
        int maxLoginPerIp,
        int idleTime,
        int uploadRate,
        int downloadRate
) {}