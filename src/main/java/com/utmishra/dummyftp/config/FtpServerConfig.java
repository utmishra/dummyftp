package com.utmishra.dummyftp.config;

import org.apache.ftpserver.FtpServer;
import org.apache.ftpserver.FtpServerFactory;
import org.apache.ftpserver.ftplet.*;
import org.apache.ftpserver.listener.ListenerFactory;
import org.apache.ftpserver.DataConnectionConfigurationFactory;
import org.apache.ftpserver.usermanager.PropertiesUserManagerFactory;
import org.apache.ftpserver.usermanager.SaltedPasswordEncryptor;
import org.apache.ftpserver.usermanager.impl.BaseUser;
import org.apache.ftpserver.usermanager.impl.WritePermission;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.ArrayList;
import java.util.List;

@Configuration
public class FtpServerConfig {

    private static final Logger logger = LoggerFactory.getLogger(FtpServerConfig.class);
    private final FtpConfigProperties ftpConfigProperties;

    public FtpServerConfig(FtpConfigProperties ftpConfigProperties) {
        this.ftpConfigProperties = ftpConfigProperties;
    }

    @Bean
    public FtpServer ftpServer() throws FtpException {
        FtpServerFactory serverFactory = new FtpServerFactory();
        ListenerFactory factory = new ListenerFactory();
        factory.setPort(21);

        DataConnectionConfigurationFactory dataConfigFactory = new DataConnectionConfigurationFactory();
        dataConfigFactory.setPassivePorts("30000-30010");
        factory.setDataConnectionConfiguration(dataConfigFactory.createDataConnectionConfiguration());

        serverFactory.addListener("default", factory.createListener());

        PropertiesUserManagerFactory userManagerFactory = new PropertiesUserManagerFactory();
        userManagerFactory.setPasswordEncryptor(new SaltedPasswordEncryptor());

        UserManager userManager = userManagerFactory.createUserManager();
        userManager.save(getConfiguredBaseUser());

        serverFactory.setUserManager(userManager);

        FtpServer server = serverFactory.createServer();
        try {
            server.start();
            logger.info("FTP server started successfully");
        }
        catch (FtpException e) {
            logger.error("Failed to start FTP server", e);
            throw e;
        }
        return server;
    }

    private BaseUser getConfiguredBaseUser() {
        BaseUser user = new BaseUser();
        List<Authority> authorityList = new ArrayList<>();

        if (ftpConfigProperties.writePermission()) {
            authorityList.add(new WritePermission());
        }

        user.setName(ftpConfigProperties.userName());
        user.setPassword(ftpConfigProperties.userPassword());
        user.setHomeDirectory(ftpConfigProperties.homeDirectory());
        user.setEnabled(ftpConfigProperties.enableFlag());
        user.setAuthorities(authorityList);
        user.setMaxIdleTime(ftpConfigProperties.idleTime());

        return user;
    }
}